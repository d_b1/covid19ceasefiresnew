function openTimeline() {
	console.log("Opening timeline");
  var i, tabcontent, tablinks;

  document.getElementById("search").style.display = "none";
  document.getElementById("map").style.display = "none";
  document.getElementById("timeline-embed").style.display = "block";
}

function openSearch() {

	console.log("Opening search");
	var i, tabcontent, tablinks;
	tabcontent = document.getElementsByClassName("tabcontent");
	for (i = 0; i < tabcontent.length; i++) {
	tabcontent[i].style.display = "none";
  }

  document.getElementById("timeline-embed").style.display = "none";
  document.getElementById("map").style.display = "none";
  document.getElementById("search").style.display = "block";
}

function openMap() {
	console.log("Opening map");
	var i, tabcontent, tablinks;
	tabcontent = document.getElementsByClassName("tabcontent");
	for (i = 0; i < tabcontent.length; i++) {
	tabcontent[i].style.display = "none";
	}

	document.getElementById("search").style.display = "none";
	document.getElementById("timeline-embed").style.display = "none";
	document.getElementById("map").style.display = "block";

	loadMap();
}
