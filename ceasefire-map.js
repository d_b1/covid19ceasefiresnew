var map;

function loadMap() {
    const mapbox_token = "pk.eyJ1IjoibWFwYm94Y3JlYXRlIiwiYSI6ImNrYTg3c3Y2bzBhYWkycm11ZDI0ZGUxengifQ.dlzwdxuzygjewgO13vAsPQ";
    mapboxgl.accessToken = mapbox_token;
    map = new mapboxgl.Map({
        container: "map",
        //style: "mapbox://styles/mapbox/dark-v10",
        style: "mapbox://styles/notalemesa/ck8dqwdum09ju1ioj65e3ql3k",
        zoom: 1.5,
        center: [0,20]
    });

    getCovidData();
    // usinfg graphql
    getAllCeasefireData();

     

    // map.on("load", function() {
    //     var data = getCeasefireData();
    // });
    
}

function getCeasefireData() {  

    //var proxyUrl = 'https://cors-anywhere.herokuapp.com/',
    var targetUrl = 'https://test.pax.peaceagreements.org/covid/api/ceasefires' ;

    var popupContentStr = '<div class="customInfobox">'+
                            '<div class="title">{title}</div>'+
                            '<div class="row" >'+
                                '<p>{body}</p>'+
                            '</div> '+
                          '</div>';

    //fetch(proxyUrl + targetUrl)
    fetch(targetUrl)
        .then((resp) => resp.json())

        .then(function(data) {

            var results = data.results;
            var title, body, pdf, sources;
            results.forEach(function(result) {
                
                var countries = result.countries;
                countries.forEach(function(country) {
                    lng = result.longitude_central;
                    lat = result.latitude_central;

                    //console.log("lng, lat : "+lng+", "+lat);

                    if(lng == null || lng == undefined || lat == null || lat == undefined) {
                        setCoordinates(country);
                    } else {
                        country.longitude = lng;
                        country.latitude = lat;
                    }
                    
                    title = country.name;
                    pdf = result.pdf;
                    sources = result.sources;


                    body = '<b> Region </b>'+result.region_entity
                     + '<br><b>Type </b> '+result.declaration_type
                     + '<br><b>Actors </b> '+result.actors;

                    if(pdf != null && pdf != undefined && pdf.length != 0) {
                        body += '<br> <a href=\''+pdf+'\' target="_blank" >View pdf</a>';
                    }

                    if(sources != null && sources != undefined && sources.length != 0) {
                        body += '<br> <a href=\''+sources+'\' target="_blank" >View sources</a>';
                    }   
                    
                    var contentString = popupContentStr.replace('{title}', title).replace('{body}', body);
                    
                    var popup = new mapboxgl.Popup() 
                    .setHTML(contentString);

                    var marker = new mapboxgl.Marker({color: 'green'})
                    .setLngLat([country.longitude, country.latitude])
                    .setPopup(popup)
                    .addTo(map);
                })
                
            })
        })
        .catch(function(error) {
            console.log(error);
        });
        
}

function setCoordinates(country) {

    console.log("In setCoordinates : "+country.name);
    switch(country.name) {
        case "United Nations": country.latitude = "37.09024";
                                country.longitude = "-95.712891";
                                break;
        case "Colombia": country.latitude = "4.570868";
                        country.longitude = "-74.297333"; 
                        break;
        case "Cameroon": country.latitude = "7.369721999999999";
                        country.longitude = "12.354722"; 
                        break;
        case "Philippines": country.latitude = "12.879721";
                            country.longitude = "121.774017"; 
                            break;
    }
}

function getCovidData() {

    var targetUrl = 'https://disease.sh/v3/covid-19/jhucsse';
    fetch(targetUrl)
        .then((resp) => resp.json())
        .then(data =>
            data.map((point, index) => ({
              type: "Feature",
              geometry: {
                type: "Point",
                coordinates: [
                  point.coordinates.longitude,
                  point.coordinates.latitude
                ]
              },
              properties: {
                id: index,
                country: point.country,
                province: point.province,
                cases: point.stats.confirmed,
                deaths: point.stats.deaths
              }
            }))
        )
        .then(function(covidData) {

            // Add navigation controls to the top right of the canvas
            map.addControl(new mapboxgl.NavigationControl());
            //console.log("covidData : "+covidData);

            map.once('load', function(){

                map.addSource("points", {
                  type: "geojson",
                  data: {
                    type: "FeatureCollection",
                    features: covidData
                  }
                });

                //Add the layer to map
                map.addLayer({
                  id: "circles",
                  source: "points", // this should be the id of source
                  type: "circle",
                  paint: {
                    "circle-opacity": 0.75,
                    "circle-stroke-width": [
                      "interpolate",
                      ["linear"],
                      ["get", "cases"],
                      1,
                      1,
                      100000,
                      1.75
                    ],
                    "circle-radius": [
                      "interpolate",
                      ["linear"],
                      ["get", "cases"],
                      1,
                      4,
                      1000,
                      8,
                      4000,
                      10,
                      8000,
                      14,
                      12000,
                      18,
                      100000,
                      40
                    ],
                    "circle-color": [
                      "interpolate",
                      ["linear"],
                      ["get", "cases"],
                      1,
                      "#ffffb2",
                      5000,
                      "#fed976",
                      10000,
                      "#feb24c",
                      25000,
                      "#fd8d3c",
                      50000,
                      "#fc4e2a",
                      75000,
                      "#e31a1c",
                      100000,
                      "#b10026"
                    ]
                  }
                });

                // Add popup
                const popup = new mapboxgl.Popup({
                  closeButton: false,
                  closeOnClick: false
                });

                let lastId;

                map.on("mousemove", "circles", e => {
                    const id = e.features[0].properties.id;

                    if (id !== lastId) {
                    lastId = id;
                    const {
                      cases,
                      deaths,
                      country,
                      province
                    } = e.features[0].properties;

                    // Change the pointer type on mouseenter
                    map.getCanvas().style.cursor = "pointer";

                    const coordinates = e.features[0].geometry.coordinates.slice();

                    const provinceHTML =
                    province !== "null" ? `<p>Province: <b>${province}</b></p>` : "";
                    const mortalityRate = ((deaths / cases) * 100).toFixed(2);

                    const HTML = `<p>Country: <b>${country}</b></p>
                        ${provinceHTML}
                        <p>Cases: <b>${cases}</b></p>
                        <p>Deaths: <b>${deaths}</b></p>
                        <p>Mortality Rate: <b>${mortalityRate}%</b></p>`;

                    // Ensure that if the map is zoomed out such that multiple
                    // copies of the feature are visible, the popup appears
                    // over the copy being pointed to.
                    while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
                      coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
                    }

                    popup
                      .setLngLat(coordinates)
                      .setHTML(HTML)
                      .addTo(map);
                    }
                });

                map.on("mouseleave", "circles", function() {
                    lastId = undefined;
                    map.getCanvas().style.cursor = "";
                    popup.remove();
                });
            });
        })
        .catch(function(error) {
            console.log(error);
        });

}    

function getAllCeasefireData() {

    console.log("In the graphql method");

    var targetUrl = 'https://test.pax.peaceagreements.org/api/gql/#query=%23%20Welcome%20to%20GraphiQL%0A%23' ;

    var popupContentStr = '<div class="customInfobox">'+
                            '<div class="title">{title}</div>'+
                            '<div class="row" >'+
                                '<p>{body}</p>'+
                            '</div> '+
                          '</div>';

    fetch(targetUrl, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
        body: JSON.stringify({query: ceasefireApiString()})
    })
    .then(r => r.json())
    .then(function(result) {
        //console.log("...result.data : "+result.data);
        var edges = result.data.allCeasefires.edges;

        var title, body, pdf, sources, node;

        edges.forEach(function(edge) {
            node = edge.node;
            //console.log("node.uid : "+node.uid);
      
                lng = node.longitudeCentral;
                lat = node.latitudeCentral;

                if(lng == null || lng == undefined || lat == null || lat == undefined) {
                  
                    node.longitudeCentral = "-95.712891";
                    node.latitudeCentral =  "37.09024";
                } 
                
                title = node.countriesFlat;
                pdf = node.pdf;
                sources = node.sources;


                body = '<b> Region </b>'+node.regionEntity
                 + '<br><b>Type </b> '+node.declarationType.name
                 + '<br><b>Actors </b> '+node.actors;

                if(pdf != null && pdf != undefined && pdf.length != 0) {
                    body += '<br> <a href=\''+pdf+'\' target="_blank" >View pdf</a>';
                }

                if(sources != null && sources != undefined && sources.length != 0) {
                    body += '<br> <a href=\''+sources+'\' target="_blank" >View sources</a>';
                }   
                
                var contentString = popupContentStr.replace('{title}', title).replace('{body}', body);
                
                var popup = new mapboxgl.Popup() 
                .setHTML(contentString);

                var marker = new mapboxgl.Marker({color: 'blue'})
                .setLngLat([node.longitudeCentral, node.latitudeCentral])
                .setPopup(popup)
                .addTo(map);
            //})
            
        })
    })
    .catch(function(error) {
        console.log(error);
    });        
}

function ceasefireApiString(){
    var str = "{"+
        "allCeasefires{ "+
            "edges{ "+
              "node{ "+
                "uid "+
                "ceasefireNumber "+
                "version "+
                "declarationDate "+
                "startDate "+
                "terminationDate "+
                "regionEntity "+
                "ucdpConflictId "+
                "longitudeCentral "+
                "latitudeCentral "+
                "reciprocated "+
                "pdf "+
                "headline "+
                "information "+
                "media "+
                "mediaCaption "+
                "mediaCredit "+
                "pastAgreements "+
                "pastCeasefires "+
                "sources "+
                "declarationType{ "+
                "  code "+
                "  name "+
                "} "+
                "countriesFlat "+
                "actors "+
                "actorTypes { "+
                "  edges { "+
                "    node { "+
                "      id "+
                "      name "+
                "    } "+
                "  } "+
                "} "+
              "} "+
            "} "+
        "} "+
    "} ";

    return str;
}
